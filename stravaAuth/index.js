const https = require('https');

const CLIENT_ID = "39609";
const CLIENT_SECRET = "secret";
const ALLOWED_ORIGINS = ['http://localhost:1234', 'https://ccmap.gitlab.io'];

module.exports = function (context) {
    const code = context.req.query.code;
    const grantType = context.req.query.grant_type;
    const refreshToken = context.req.query.refresh_token;
    const origin = context.req.headers.origin;
    if (!ALLOWED_ORIGINS.includes(origin)) {
        context.res = { status: 403 };
        context.done();
        return;
    }

    let path = `/oauth/token?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&grant_type=${grantType}`;
    if (code) {
        path += `&code=${code}`;
    }
    if (refreshToken) {
        path += `&refresh_token=${refreshToken}`;
    }

    const options = {
        hostname: 'www.strava.com',
        port: 443,
        path: path,
        method: 'POST',
        headers: {}
    };

    const req = https.request(options, res => {
        res.on('data', d => {
            context.res = {
                status: res.statusCode,
                headers: {
                    'Access-Control-Allow-Origin': origin,
                },
                body: JSON.stringify(JSON.parse(d)),
            };
            context.done();
        });
    });
    req.on('error', () => {
        context.res = { status: 500 };
        context.done();
    });
    req.end();
};
